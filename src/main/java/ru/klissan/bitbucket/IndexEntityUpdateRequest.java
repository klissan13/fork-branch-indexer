package ru.klissan.bitbucket;

import com.atlassian.bitbucket.repository.MinimalRef;
import com.atlassian.bitbucket.repository.Repository;

public class IndexEntityUpdateRequest {
    private final Repository repository;
    private final MinimalRef ref;
    private final Iterable<String> issues;

    public IndexEntityUpdateRequest(Repository repository, MinimalRef ref, Iterable<String> issues) {
        this.repository = repository;
        this.ref = ref;
        this.issues = issues;
    }

    public int getRepoId() {
        return repository.getId();
    }

    public String getRefId() {
        return ref.getId();
    }

    public Iterable<String> getIssues() {
        return issues;
    }
}
