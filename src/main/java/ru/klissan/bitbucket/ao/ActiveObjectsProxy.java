package ru.klissan.bitbucket.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import net.java.ao.RawEntity;
import net.java.ao.schema.Table;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ActiveObjectsProxy {
    private final ActiveObjects aoService;
    public Map<String, Class<? extends RawEntity<?>>> entitiesByTable;


    public ActiveObjectsProxy(Set<Class<? extends RawEntity<?>>> entities, ActiveObjects aoService) {
        this.aoService = aoService;
        entitiesByTable = entities.stream().collect(Collectors.toMap(
            //TODO convert class name to table name like in AO
            clazz -> clazz.isAnnotationPresent(Table.class) ? clazz.getAnnotation(Table.class).value() :
                clazz.getSimpleName(),
            Function.identity()));
    }

    //TODO: more flexible
    public ProxyEntity create(String tableName, Map<String, Object> params) {
        Class<? extends RawEntity> clazz = getClassForTable(tableName);
        return new ProxyEntity(aoService.create(clazz, params), tableName);
    }

    public ProxyEntity get(String tableName, Long id) {
        return new ProxyEntity(getRawEntity(tableName, id), tableName);
    }

    public List<ProxyEntity> find(String tableName, String criteria, Object... params) {
        RawEntity[] rawEntities = aoService.find(getClassForTable(tableName), criteria, params);
        if (rawEntities == null) {
            return Collections.emptyList();
        }
        return Stream.of(rawEntities).map(r -> new ProxyEntity(r, tableName))
            .collect(Collectors.toList());
    }

    public void delete(String tableName, Long id) {
        RawEntity entity = getRawEntity(tableName, id);
        if (entity != null) {
            aoService.delete(entity);
        }
    }

    private RawEntity getRawEntity(String tableName, Long id) {return aoService.get(getClassForTable(tableName), id);}


    private Class<? extends RawEntity> getClassForTable(String tableName) {
        if (!entitiesByTable.containsKey(tableName)) {
            throw new IllegalArgumentException("Unknown table name: " + tableName);
        }

        return entitiesByTable.get(tableName);
    }

}
