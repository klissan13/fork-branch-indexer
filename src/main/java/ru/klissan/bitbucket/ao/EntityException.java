package ru.klissan.bitbucket.ao;

public class EntityException extends Exception {
    public EntityException(Throwable cause) {
        super(cause);
    }
}
