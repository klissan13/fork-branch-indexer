package ru.klissan.bitbucket.ao;

import net.java.ao.RawEntity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//cache methods??
public class ProxyEntity {

    private final Class<? extends RawEntity> clazz;
    private final RawEntity<?> entity;
    private final String tableName;

    public ProxyEntity(RawEntity<?> entity, String tableName) {
        this.entity = entity;
        this.tableName = tableName;
        this.clazz = entity.getClass();
    }

    public void set(String setter, Object value) throws EntityException {
        try {
            Method method = clazz.getMethod(setter, value.getClass());
            method.invoke(entity, value);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new EntityException(e);
        }
    }

    public <T> T  get(String getter, Class<T> expectedClass) throws EntityException {
        try {
            Method method = clazz.getMethod(getter);
            return expectedClass.cast(method.invoke(entity));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new EntityException(e);
        }
    }

    public void save() {
        entity.save();
    }

    public String getTableName() {
        return tableName;
    }
}
