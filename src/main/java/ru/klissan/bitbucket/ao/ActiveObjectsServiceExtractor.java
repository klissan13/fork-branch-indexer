package ru.klissan.bitbucket.ao;

import com.atlassian.activeobjects.config.ActiveObjectsConfiguration;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceReference;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

@Component
public class ActiveObjectsServiceExtractor {
    private static final     Logger log = Logger.getLogger(ActiveObjectsServiceExtractor.class);
    public static final String STANDARD_AO_PLUGIN_KEY = "com.atlassian.activeobjects.activeobjects-plugin";
    public static final String BACKUP_AO_SERVICE_CLASS = "com.atlassian.activeobjects.spi.Backup";

    private final PluginAccessor pluginAccessor;
    private ServiceFactory aoFactory;

    @Inject
    public ActiveObjectsServiceExtractor(@ComponentImport PluginAccessor pluginAccessor) throws Exception {
        this.pluginAccessor = pluginAccessor;
        validate();
    }

    private void validate() throws Exception {
        Optional<Plugin> aoPlugin =
            pluginAccessor.getEnabledPlugins().stream().filter(plugin -> plugin.getKey().equals(STANDARD_AO_PLUGIN_KEY)).findFirst();
        if(!aoPlugin.isPresent()) {
            throw new IllegalPluginStateException("Active Objects Plugin does not exist: " + STANDARD_AO_PLUGIN_KEY);
        }

        Class<?> backupAoServiceClass = aoPlugin.get().getClassLoader().loadClass(BACKUP_AO_SERVICE_CLASS);
        OsgiPlugin aoOsgiPlugin = (OsgiPlugin) aoPlugin.get();
        BundleContext bundleContext = aoOsgiPlugin.getBundle().getBundleContext();
        ServiceReference<?> backupServiceReference = bundleContext.getServiceReference(backupAoServiceClass);
        if(backupServiceReference == null) {
            throw new IllegalPluginStateException("Backup service not found");
        }

        Object backupService = bundleContext.getService(backupServiceReference);

        Field aoServiceFactoryField = backupService.getClass().getDeclaredField("aoServiceFactory");
        aoServiceFactoryField.setAccessible(true);
        aoFactory = (ServiceFactory) aoServiceFactoryField.get(backupService);
    }

    public ActiveObjectsProxy getAoForPlugin(String pluginKey) {
        OsgiPlugin plugin = (OsgiPlugin) pluginAccessor.getEnabledPlugin(pluginKey);
        ActiveObjects aoService = (ActiveObjects) aoFactory.getService(plugin.getBundle(), null);
        //Maybe not null, may be exception or something else
        if(aoService == null) {
            throw new IllegalArgumentException("Plugin " + pluginKey + " does not have AO service");
        }

        Optional<ModuleDescriptor<?>> aoModule =
            plugin.getModuleDescriptors().stream().filter(p -> "com.atlassian.activeobjects.plugin.ActiveObjectModuleDescriptor".equals(p.getClass().getName()))
                .findFirst();

        ModuleDescriptor activeObjectModuleDescriptor = aoModule.orElseThrow(() -> new IllegalArgumentException("Plugin " + pluginKey + " does not have AO module"));
        try {
            Method method = activeObjectModuleDescriptor.getClass().getMethod("getConfiguration");
            ActiveObjectsConfiguration configuration =
                (ActiveObjectsConfiguration) method.invoke(activeObjectModuleDescriptor);
            return new ActiveObjectsProxy(configuration.getEntities(), aoService);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new IllegalArgumentException("Plugin " + pluginKey + " has incorrec AO module");
        }
    }

}
