package ru.klissan.bitbucket;

import com.atlassian.bitbucket.event.repository.RepositoryRefsChangedEvent;
import com.atlassian.bitbucket.project.ProjectType;
import com.atlassian.bitbucket.repository.MinimalRef;
import com.atlassian.bitbucket.repository.Ref;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.ResolveRefRequest;
import com.atlassian.bitbucket.repository.StandardRefType;
import com.atlassian.devstatus.IssueChangedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.integration.jira.JiraKeyScanner;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.component.ComponentLocator;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.klissan.bitbucket.ao.EntityException;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Sets.newHashSet;

@Component
@SuppressWarnings("unused")
public class
RepositoryRefsChangedListener {
    private final static Logger log = Logger.getLogger(RepositoryRefsChangedListener.class);
    private final JiraKeyScanner jiraKeyScanner;
    private final RefService refService;
    private final EventPublisher eventPublisher;
    private final ExecutorService executorService;
    private final EventConverter eventConverter;
    private final IndexEntityManager indexEntityManager;


    @Inject
    public RepositoryRefsChangedListener(@ComponentImport JiraKeyScanner jiraKeyScanner,
        @ComponentImport RefService refService, @ComponentImport EventPublisher eventPublisher,
        @ComponentImport ExecutorService executorService, EventConverter eventConverter,
        IndexEntityManager indexEntityManager)
    {
        this.jiraKeyScanner = jiraKeyScanner;
        this.refService = refService;
        this.eventPublisher = eventPublisher;
        this.executorService = executorService;
        this.eventConverter = eventConverter;
        this.indexEntityManager = indexEntityManager;
    }

    @EventListener
    public void onRepositoryRefsChanged(RepositoryRefsChangedEvent event) {
        Repository repository = event.getRepository();
        if (repository.isFork() && repository.getProject().getType() != ProjectType.PERSONAL) {
            executorService.submit(() -> {
                try {
                    process(event);
                } catch (Exception e) {
                    log.error("Something wrong", e);
                }
            });
        }
    }

    private void process(RepositoryRefsChangedEvent event) throws EntityException {
        Repository repository = event.getRepository();
        Set<String> allIssues = new HashSet<>();
        Collection<RefChange> branchChanges = extractBranchChanges(event);

            for (RefChange refChange : branchChanges) {
                MinimalRef ref = refChange.getRef();
                Iterable<String> issues = getIssues(refChange);
                if (!isEmpty(issues)) {
                    if (refChange.getType() == RefChangeType.ADD && notExistsInParentRepo(repository, ref)) {
                        indexEntityManager.create(new IndexEntityUpdateRequest(repository, ref, issues));
                        //fire JiraBranchEvent remote event for triggers
                        eventConverter.convertToBranchCreated(event, refChange, issues)
                            .ifPresent(eventPublisher::publish);
                    } else if (refChange.getType() == RefChangeType.DELETE) {
                        indexEntityManager.delete(new IndexEntityUpdateRequest(repository, ref, issues));
                    } else if (refChange.getType() == RefChangeType.UPDATE) {
                        indexEntityManager.update(new IndexEntityUpdateRequest(repository, ref, issues));
                    }
                    allIssues.addAll(newHashSet(issues));
                }
            }

        if (!allIssues.isEmpty()) {
            eventPublisher.publish(new IssueChangedEvent(allIssues));
        }
    }

    private List<RefChange> extractBranchChanges(RepositoryRefsChangedEvent event) {
        return event.getRefChanges()
            .stream()
            .filter(change -> change.getRef().getType() == StandardRefType.BRANCH).collect(Collectors.toList());
    }

    private Iterable<String> getIssues(RefChange refChange) {
        return jiraKeyScanner.findAll(refChange.getRef().getDisplayId());
    }

    private boolean notExistsInParentRepo(Repository repository, MinimalRef ref) {
        Repository origin = repository.getOrigin();
        if (origin == null) {
            return true;
        }
        Ref originRef = refService.resolveRef(
            new ResolveRefRequest.Builder(origin).type(ref.getType()).refId(ref.getId()).build());
        return originRef == null;
    }
}
