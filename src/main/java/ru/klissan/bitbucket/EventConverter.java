package ru.klissan.bitbucket;

import com.atlassian.bitbucket.event.repository.RepositoryRefsChangedEvent;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.devstatus.EventEntity;
import com.atlassian.devstatus.EventInitiator;
import com.atlassian.devstatus.vcs.JiraBranchEvent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@SuppressWarnings("unused")
public class EventConverter {
    private final NavBuilder navBuilder;

    @Inject
    public EventConverter(@ComponentImport NavBuilder navBuilder) {
        this.navBuilder = navBuilder;
    }

    public Optional<JiraBranchEvent> convertToBranchCreated(RepositoryRefsChangedEvent event,
        RefChange refChange, Iterable<String> issues)
    {
        try {
            return Optional.of(new JiraBranchEvent(
                getInitiator(event.getUser()),
                toEventEntity(event, refChange),
                StreamSupport.stream(issues.spliterator(), false).collect(Collectors.toSet()),
                refChange.getFromHash(),
                refChange.getToHash(),
                JiraBranchEvent.Type.CREATE
            ));
        } catch (URISyntaxException e) {
            return Optional.empty();
        }
    }

    private EventEntity toEventEntity(RepositoryRefsChangedEvent event, RefChange refChange) throws URISyntaxException
    {
        return EventEntity.newNonUniqueEntity(refChange.getRef().getDisplayId(), getRefUri(event, refChange));
    }

    private URI getRefUri(RepositoryRefsChangedEvent event, RefChange refChange) throws URISyntaxException {
            return new URI(
                navBuilder.repo(event.getRepository()).commits().withParam("until", refChange.getRef().getId())
                    .buildAbsolute());
    }

    private EventInitiator getInitiator(ApplicationUser user) {
        try {
            return new EventInitiator(user.getName(), user.getDisplayName(),
                new URI(navBuilder.user(user).buildAbsolute()),
                Collections.singleton(user.getEmailAddress()),
                new URI(navBuilder.user(user).avatar(128).buildAbsolute()));
        } catch (URISyntaxException e) {
            return EventInitiator.EMPTY_INITIATOR;
        }
    }

}
