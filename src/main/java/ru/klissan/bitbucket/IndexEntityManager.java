package ru.klissan.bitbucket;

import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;
import ru.klissan.bitbucket.ao.ActiveObjectsProxy;
import ru.klissan.bitbucket.ao.ActiveObjectsServiceExtractor;
import ru.klissan.bitbucket.ao.EntityException;
import ru.klissan.bitbucket.ao.ProxyEntity;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Component
public class IndexEntityManager {
    public static final String PLUGIN_KEY = "com.atlassian.bitbucket.server.bitbucket-jira-development-integration";
    public static final String TABLE_NAME = "JIRA_INDEX";

    public static final String REPOSITORY_COLUMN_NAME = "REPOSITORY";
    public static final String ISSUE_COLUMN_NAME = "ISSUE";
    public static final String BRANCH_COLUMN_NAME = "BRANCH";
    public static final String UPDATED_COLUMN_NAME = "LAST_UPDATED";

    private final ActiveObjectsServiceExtractor serviceExtractor;

    @Inject
    public IndexEntityManager(ActiveObjectsServiceExtractor serviceExtractor) {
        this.serviceExtractor = serviceExtractor;
        //get ao for every request. It more safely, but worse performance
    }


    public void create(IndexEntityUpdateRequest request) {
        ActiveObjectsProxy aoForPlugin = serviceExtractor.getAoForPlugin(PLUGIN_KEY);
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.<String, Object>builder()
            .put(REPOSITORY_COLUMN_NAME, request.getRepoId())
            .put(BRANCH_COLUMN_NAME, request.getRefId())
            .put(UPDATED_COLUMN_NAME, new Date());

        for (String issue : request.getIssues()) {
            aoForPlugin.create(TABLE_NAME, builder.put(ISSUE_COLUMN_NAME, issue).build());
        }
    }

    public void update(IndexEntityUpdateRequest request) throws EntityException {
        ActiveObjectsProxy aoForPlugin = serviceExtractor.getAoForPlugin(PLUGIN_KEY);
        for (String issue : request.getIssues()) {
            List<ProxyEntity> entities = findEntities(aoForPlugin, request, issue);
            for (ProxyEntity entity : entities) {
                entity.set("setUpdatedDate", new Date());
                entity.save();
            }
        }
    }


    public void delete(IndexEntityUpdateRequest request) throws EntityException {
        ActiveObjectsProxy aoForPlugin = serviceExtractor.getAoForPlugin(PLUGIN_KEY);
        for (String issue : request.getIssues()) {
            List<ProxyEntity> entities = findEntities(aoForPlugin, request, issue);
            for (ProxyEntity entity : entities) {
                aoForPlugin.delete(TABLE_NAME, entity.get("getId", Long.class));
            }
        }

    }

    private List<ProxyEntity> findEntities(ActiveObjectsProxy aoForPlugin, IndexEntityUpdateRequest request,
        String issue)
    {
        return aoForPlugin.find(TABLE_NAME,
            REPOSITORY_COLUMN_NAME + " = ? AND " + ISSUE_COLUMN_NAME + " = ? AND " + BRANCH_COLUMN_NAME + " = ?" +
                " AND PR_ID is null",
            request.getRepoId(), issue, request.getRefId());
    }

}
